import PropTypes from "prop-types";
import React from "react";
import { ReactComponent as LessIcon } from "../../assets/less.svg";
import { ReactComponent as PlusIcon } from "../../assets/plus.svg";
import * as Styles from "./styles";

const Card = ({ title, subtitle, smallSubtitle, photo, description }) => {
  const [showInfo, setShowInfo] = React.useState(false);
  return (
    <Styles.Card showInfo={showInfo} hasPhoto={!!photo}>
      <img src={photo} alt={`no_photo_${title}`} />
      <Styles.CardBody className="card-body">
        <span>{title}</span>
        <h5>{subtitle}</h5>
        <p>{smallSubtitle}</p>
      </Styles.CardBody>
      <Styles.PlusContainer
        className="plus"
        showInfo={showInfo}
        onClick={() => setShowInfo(!showInfo)}
      >
        <h6>{showInfo ? "Menos" : "Mais"} informações</h6>
        {showInfo ? <LessIcon /> : <PlusIcon />}
      </Styles.PlusContainer>
      {showInfo && (
        <Styles.Description className="description">
          {description}
        </Styles.Description>
      )}
    </Styles.Card>
  );
};

Card.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  smallSubtitle: PropTypes.string,
  photo: PropTypes.string,
  description: PropTypes.string,
};

Card.defaultProps = {
  title: "Title",
  subtitle: "Subtitle",
  smallSubtitle: "Other",
  photo: "",
  description:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque accumsan quis sem a molestie. Curabitur ac nisi at quam consectetur aliquet.",
};

export default Card;
