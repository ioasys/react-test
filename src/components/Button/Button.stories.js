import React from "react";
import { ThemeProvider } from "styled-components";
import Button from ".";
import theme from "../../styles/theme";

export const Sample = (args) => (
  <ThemeProvider theme={theme}>
    <Button {...args} />
  </ThemeProvider>
);

const story = {
  title: "Button",
  component: Sample,
  argTypes: {
    children: {
      control: "text",
      type: { name: "string", required: false },
      defaultValue: "TEXT",
      description: "_O que é renderizado dentro do botão_",
      table: {
        defaultValue: {
          summary: "TEXT",
          detail: "By default its set to 'TEXT'",
        },
      },
    },
  },
};

export default story;
