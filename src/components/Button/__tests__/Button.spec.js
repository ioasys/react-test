import { render, screen } from "@testing-library/react";
import { ThemeProvider } from "styled-components";
import Button from "..";
import theme from "../../../styles/theme";

const makeSut = (props) => {
  return render(
    <ThemeProvider theme={theme}>
      <Button {...props} />
    </ThemeProvider>
  );
};

it("should render button text", async () => {
  const component = makeSut({ children: "button-test-children" });
  const children = component.getByText("button-test-children");
  expect(children).toBeTruthy();
});

it("should work with custom onClick function", async () => {
  const clickFunction = jest.fn();
  makeSut({ onClick: clickFunction });
  const btn = screen.getByRole("button");
  btn.click();
  expect(clickFunction).toHaveBeenCalledTimes(1);
});

it("should work without custom onClick function", async () => {
  const component = makeSut();
  expect(Button.defaultProps.onClick).toBeDefined();
  screen.getByRole("button").click();
  expect(component.baseElement).toHaveFocus();
});
