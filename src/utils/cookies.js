/* eslint-disable no-useless-escape */
/* eslint-disable camelcase */
/* eslint-disable no-unused-expressions */

/**
 * @typedef {Object} Options
 * @property {number} expire_in - tempo em ms para expiração.
 *
 * @description Set new cookie
 * @param {string} key - Chave do cookie
 * @param {string} value - Valor do cookie
 * @param {Options} options - Opções para o cookie
 */
export const setCookies = (key, value, options) => {
  options?.expire_in
    ? (document.cookie = `${key}=${value};${new Date(
        Date.now() + options.expire_in
      ).toUTCString()};path=/`)
    : (document.cookie = `${key}=${value}`);
};
/**
 * @description Delete cookie
 * @param {string} key - Chave do cookie
 */
export const deleteCookies = (key) => {
  document.cookie = `${key}=;expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
};

/**
 * @description Find cookie
 * @param {string} key - Chave do cookie
 */

export const getCookie = (key) => {
  if (!key.trim()) {
    return new Error("The fields key and value is missing");
  }

  const name = `${key}=`;
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(";");
  for (let i = 0; i < ca.length; i += 1) {
    let c = ca[i];
    while (c.charAt(0) === " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
};
