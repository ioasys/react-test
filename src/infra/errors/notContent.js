export class DontContentError extends Error {
  constructor(request) {
    super(`Dont Content: ${request}`);
    this.name = 'DontContentError';
  }
}
