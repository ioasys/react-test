import styled from "styled-components";

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  text-align: center;

  h4 {
    font-family: ${({ theme: { fonts } }) => fonts.textStyle10.family};
    font-size: ${({ theme: { fonts } }) => fonts.textStyle10.size};
    font-weight: ${({ theme: { fonts } }) => fonts.textStyle10.weight};
    color: ${({ theme: { colors } }) => colors.charcoalGrey};

    width: 11rem;

    margin-top: 4.188rem;
  }

  p {
    font-family: ${({ theme: { fonts } }) => fonts.textStyle1.family};
    font-size: ${({ theme: { fonts } }) => fonts.textStyle1.size};
    font-weight: ${({ theme: { fonts } }) => fonts.textStyle1.weight};
    color: ${({ theme: { colors } }) => colors.charcoalGrey};

    width: 22.36rem;

    margin-top: 1.281rem;
    margin-bottom: 2.969rem;
  }
`;
