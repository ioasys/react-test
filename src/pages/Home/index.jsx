import React from "react";
import { ReactComponent as SearchIcon } from "../../assets/ic-search-copy.svg";
import NavLogo from "../../assets/logo-nav.png";
import Card from "../../components/Card";
import Input from "../../components/Input";
import Loading from "../../components/Loading";
import makeEnterpriseService from "../../infra/factories/enterprise";
import * as Styles from "./styles";

const Home = () => {
  const [enterprises, setEnterprises] = React.useState([]);
  const [filter, setFilter] = React.useState([]);
  const [expandSearch, setExpandSearch] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const [search, setSearch] = React.useState("");
  const [searchCount, setSearchCount] = React.useState();

  React.useEffect(() => {
    async function fetchEnterprises() {
      const api = makeEnterpriseService();
      const response = await api.getAll();
      setEnterprises(response?.body?.enterprises);
    }

    fetchEnterprises();
  }, []);

  const data = filter.length && search ? filter : enterprises;

  async function handleSearch(event) {
    setSearch(event.target.value);
    setIsLoading(true);

    let searchResult = enterprises.filter(({ enterprise_name }) =>
      enterprise_name.includes(event.target.value)
    );

    if (!searchResult.length) {
      const api = makeEnterpriseService();
      const response = await api.getOne(event.target.value);
      searchResult = response?.body?.enterprises;
    }

    setSearchCount(searchResult.length);
    setFilter(searchResult);
    setIsLoading(false);
  }

  return (
    <Styles.Container expandSearch={expandSearch}>
      <nav>
        <img src={NavLogo} alt="ioasys" />
        {expandSearch && (
          <Input
            type="text"
            placeholder="Pesquisar"
            name="search"
            value={search}
            onChange={handleSearch}
            onBlur={(e) => handleSearch(e) && setExpandSearch(false)}
          />
        )}
        <SearchIcon onClick={() => setExpandSearch(!expandSearch)} />
      </nav>
      {search && (
        <h4>
          Encontramos {searchCount} resultado(s) para "{search}"
        </h4>
      )}
      {isLoading && <Loading />}
      {data?.map(
        ({
          id,
          enterprise_name,
          photo,
          description,
          enterprise_type: { enterprise_type_name },
          city,
          country,
        }) => (
          <Card
            key={`${id}_${enterprise_name}`}
            title={enterprise_name}
            photo={`https://empresas.ioasys.com.br${photo}`}
            description={description}
            subtitle={enterprise_type_name}
            smallSubtitle={`${city}-${country}`}
          />
        )
      )}
    </Styles.Container>
  );
};

export default Home;
