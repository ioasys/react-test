# Teste técnico ioasys 🚀 
# Rodando o projeto

1. Instale as dependências com **yarn** ou **npm**

```bash
    yarn
    # ou
    npm install
```

2. Iniciar o projeto no navegador

```bash
    yarn start
    # ou
    npm run start
```

3. Iniciar o storybook

```bash
    yarn storybook
    # ou
    npm run storybook
```

4. Rodar os testes

```bash
    yarn test
    # ou
    npm run test
```
